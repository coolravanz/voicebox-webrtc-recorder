# Webrtc recorder #

This application help users to record their audio and upload to s3 bucket through webrtc capable browsers 

### How to install ###

clone the source from this repository to the root of https server

```
#!python

git clone [repository url ].git
```

```
#!python

should give your accesskey , secret key ,region and bucket name in index file
```
```
#!python

should add following bucket plicy in your s3 bucket property
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Sid": "AllowPublicRead",
			"Effect": "Allow",
			"Principal": {
				"AWS": "*"
			},
			"Action": [
				"s3:PutObject",
				"s3:GetObject",
				"s3:DeleteObject"
			],
			"Resource": "arn:aws:s3:::bucketname/*"
		}
	]
}
```

```
#!python

should add the following cors configuration in your s3 bucket property

<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
    <CORSRule>
        <AllowedOrigin>your-domain</AllowedOrigin>
        <AllowedMethod>GET</AllowedMethod>
        <AllowedMethod>POST</AllowedMethod>
        <AllowedMethod>PUT</AllowedMethod>
        <AllowedMethod>DELETE</AllowedMethod>
        <AllowedMethod>HEAD</AllowedMethod>
        <MaxAgeSeconds>3000</MaxAgeSeconds>
        <AllowedHeader>Content-*</AllowedHeader>
        <AllowedHeader>*</AllowedHeader>
    </CORSRule>
</CORSConfiguration>
```

Load application [https://[your domain]/webrtc-recorder](Link URL)[Link Text](Link URL)
You can record upload and download recordings 