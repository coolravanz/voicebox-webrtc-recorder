var accessKey   =  'AKIAJNRRNFZH4PYS3AAA';
var secretKey   =  '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';
var bucketName  =  'transcription-stage';
var region      =  'us-west-1';
var s3Signature = '';
var base64Policy= '';
var folder      = ""; // overwrite with your folder
var expiration  = "2020-01-01T00:00:00Z"; // overwrite date
var date        = "20150927"; // overwrite date
var serviceName = "s3";
//----------------------------------------------------------
var port     =  8080;
var https    = require("https");              // http server core module
var express  = require("express");           // web framework external module
var io       = require("socket.io");         // web socket external module
var crypto = require("crypto");
// Setup and configure Express http server. Expect a subfolder called "static" to be the web root.
var httpApp = express();
httpApp.use(express.static(__dirname + "/static/"));

var fs = require('fs');
var options = {
    key: fs.readFileSync('cert/server.key'),
    cert: fs.readFileSync('cert/server.crt')
};
// Start Express http server on port 90
var webServer = https.createServer(options, httpApp).listen(port);
var io = require('socket.io').listen(webServer, {
        log: false,
        origins: '*:*'
    });

    io.set('transports', [
        'websocket', // 'disconnect' EVENT will work only with 'websocket'
        'xhr-polling',
        'jsonp-polling'
    ]);
	io.on('connection', function(socket) {
	    socket.on('getKeys', function () 
		{
			var data                  = {};
			    data['access']        = accessKey;
			    data['secret']        = secretKey; 
                data['bucketName']    = bucketName;
                data['region']        = region; 
                data['policy']        = base64Policy;
                data['signature']     = s3Signature; 
			socket.emit('onKeys', data);
		});
	});

    var s3Policy = {"expiration": expiration,
      "conditions": [
       {"bucket": bucketName},
       {"acl": "private"},
       ["starts-with", "$key", ""],
       {"success_action_status": "201"}
      ]
    };
    var policyjson   = new Buffer(JSON.stringify(s3Policy), "utf-8");
    var base64Policy = policyjson.toString("base64");
    //console.log('base64Policy:', base64Policy);

    //var signatureKey = getSignatureKey(secretKey, date, region, serviceName);
    s3Signature = getSignature(base64Policy, secretKey);
    function getSignature(policy, secret) {
          if (!secret) throw new Error('secret required');

          return crypto
            .createHmac('sha1', secret)
            .update(policy)
            .digest('base64');
    }
    

